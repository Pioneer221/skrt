from flask import Flask, render_template, request
import matplotlib.pyplot as plt
import datetime, io, base64, os.path

# import w1thermsensor

# sensor = w1thermsensor.W1ThermSensor()
app = Flask(__name__)

if __name__ == '__main__':
    app.run()


@app.route('/', methods=['POST', 'GET'])
def index():
    if request.method == 'POST':
        target_temp = request.form['content']
        return render_template('index.html', target_temp=target_temp)
    else:
        return render_template('index.html')


@app.route('/plot')
def build_plot():
    img = io.BytesIO()
    times, temperatures = [], []
    if os.path.isfile('readings/pomiary' + getToday() + '.txt'):
        file = open('readings/pomiary' + getToday() + '.txt', 'r')
        for line in file:
            values = line.split()
            times.append(values[0])
            temperatures.append(float(values[1]))

        # naming the x axis
        plt.xlabel('Czas')
        # naming the y axis
        plt.ylabel('Temperatura (*C)')
        # giving a title to my graph
        plt.title('Temperatura w czasie pomiaru')
        plt.plot(times, temperatures)
        # beautify the x-labels
        plt.gcf().autofmt_xdate()
        plt.savefig(img, format='png')
        img.seek(0)
        plot_url = base64.b64encode(img.getvalue()).decode()
        return 'data:image/png;base64,{}'.format(plot_url)
    else:
        print("File not exist")
        return ""


@app.route('/temperature', methods=['GET'])
def get_room_temperature():
    # temp = sensor.get_temperature()
    temp = 27.6
    saveTempWithTimeToFile(temp)
    return str(temp)


@app.route('/time', methods=['GET'])
def nowTime():
    now = datetime.datetime.now()
    timeString = now.strftime("%H:%M:%S  %d.%m.%Y")
    return timeString


def getToday():
    now = datetime.datetime.now()
    todayString = now.strftime("-%d-%m-%Y")
    return todayString


def getTimeNow():
    now = datetime.datetime.now()
    timeNowString = now.strftime("%H:%M:%S")
    return timeNowString


# ta funkcja test do usunięcia
# @app.route('/plik')
# def test():
#     temp = 27.3  # temp będzie arg wejściowym funkcji z rasberry pi
#     saveTempWithTimeToFile(temp)
#     return "ok"


def saveTempWithTimeToFile(temp):
    file = open('readings/pomiary' + getToday() + '.txt', 'a')
    file.writelines(getTimeNow() + '\t' + str(temp) + '\n')
    file.close()
