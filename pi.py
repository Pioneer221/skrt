import RPi.GPIO as GPIO
import time
import pidcontroller
import json
from time import sleep
import w1thermsensor

GPIO.setmode(GPIO.BCM)
GPIO.setup(25, GPIO.OUT)
PWM = GPIO.PWM(25, 50)
GPIO.setwarnings(False)
sensor = w1thermsensor.W1ThermSensor()
PWM.start(0)


# while(True):
#   temp = sensor.get_temperature()
#  print(temp)
# if (temp>40):
#    GPIO.output(17,GPIO.LOW)
#   print('NISKI')
# elif(temp<50):
#   GPIO.output(17,GPIO.HIGH)
#  print('WYSOKI')
def set_heater_power(power):
    if power > 10:
        power = 10
    if power < 0:
        power = 0
    PWM.ChangeDutyCycle((power * 10))


def get_room_temperature():
    temp = sensor.get_temperature()
    return temp


def keep_room_warm():
    pid = pidcontroller.PID(5, 0.000005, 0.00006)
    target_temperature = 70
    while (True):
        current_temperature = get_room_temperature()
        y = {"temperatura": current_temperature}
        j_son = json.dumps(y)
        dane = [str(get_room_temperature()), '\n']
        plik = open('plik.txt', 'a')
        plik.writelines(dane)
        error = target_temperature - current_temperature
        correction = pid.Update(error)
        print('BŁĄD REGULACJI : %f' % correction)
        print('TEMPERATURA WYNOSI : %f' % get_room_temperature())
        print(j_son)
        set_heater_power(correction)
        time.sleep(1.5)


def przyjmowanie_json(jsn):
    qq = json.loads(jsn)
    return (qq["temp_zadana"])


while (True):
    keep_room_warm()
