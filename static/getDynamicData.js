
    function addTimer() {
        getTime()
        getTemperature()
        setInterval(getTime, 1000)
        setInterval(getTemperature, 10000)
    }


//TIME
    function getCurrentTime(){
                var xmlHttp = new XMLHttpRequest();
                xmlHttp.open( "GET", "http://127.0.0.1:5000/time", false );
                xmlHttp.send( null );
                return xmlHttp.responseText;
    }

    function getTime() {
        var time = getCurrentTime();
        document.getElementById("timeField").innerText = time;
    }

//TEMPERATURE
    function getCurrentTemperature(){
                var xmlHttp = new XMLHttpRequest();
                xmlHttp.open( "GET", "http://127.0.0.1:5000/temperature", false );
                xmlHttp.send( null );
                return xmlHttp.responseText;
    }

    function getTemperature() {
        var temperature = getCurrentTemperature();
        document.getElementById("readTemp").innerText = temperature;
        getPlot()
    }

// PLOT
    function getCurrentTemperaturePlot(){
            var xmlHttp = new XMLHttpRequest();
            xmlHttp.open( "GET", "http://127.0.0.1:5000/plot", false );
            xmlHttp.send( null );
            return xmlHttp.responseText;
    }

    function getPlot(){
            var plot_src = getCurrentTemperaturePlot();
            if (plot_src!==""){
                document.getElementById("plot_id").src = plot_src;
            }
    }